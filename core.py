# lusty is srid's pet IRC bot
# cuddle, but not annoy her.
# $ python lusty.py channel channel.log

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from twisted.python import log

import time, sys

import plugins


class IRCBot(irc.IRCClient):
    """An IRC bot."""
    
    nickname = "lusty"
    realname = "Lusty"
    
    def connectionMade(self):
        irc.IRCClient.connectionMade(self)
        log.msg("[connected at %s]" % 
                        time.asctime(time.localtime(time.time())))

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)
        log.msg("[disconnected at %s]" % 
                        time.asctime(time.localtime(time.time())))


    # callbacks for events

    def signedOn(self):
        for channel in self.factory.channels:
            self.join(channel)

    def joined(self, channel):
        log.msg("[JOIN %s]" % channel)

    def privmsg(self, user, channel, msg):
        """This will get called when the bot receives a message."""
        user = user.split('!', 1)[0]
        log.msg("<%s> %s" % (user, msg))

        plugins.handleMsg(self, user, channel, msg)
        return
    
        # Check to see if they're sending me a private message
        if channel == self.nickname:
            msg = "It isn't nice to whisper!  Play nice with the group."
            self.msg(user, msg)
            log.msg('pwned!!', user, msg)
            return

        matchedRest = util.prefixMatch(msg, [self.nickname + ':', ','])
        if matchedRest is not None:
            if matchedRest == 'reload':
                reload(commands)
                log.msg('reloaded commands.py')
                return
            else:
                respChannel, responses = commands.handleCommand(
                    user, channel, matchedRest)
        else:
            respChannel, responses = commands.handleGeneric(user, channel, msg)

        for r in responses:
            self.msg(respChannel, r)
            log.msg("<%s> %s" % (self.nickname, r))
        if not responses:
            log.msg('No bot response for %s' % msg)

            
    def action(self, user, channel, msg):
        """This will get called when the bot sees someone do an action."""
        user = user.split('!', 1)[0]
        log.msg("* %s %s" % (user, msg))

    def irc_NICK(self, prefix, params):
        """Called when an IRC user changes their nickname."""
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        log.msg("%s NICK %s" % (old_nick, new_nick))


class IRCBotFactory(protocol.ClientFactory):

    protocol = IRCBot

    def __init__(self, channels):
        self.channels = channels

    def clientConnectionLost(self, connector, reason):
        connector.connect() # reconnect

    def clientConnectionFailed(self, connector, reason):
        print "connection failed:", reason
        reactor.stop()


if __name__ == '__main__':
    channels = sys.argv[1:]
    print channels
    
    log.startLogging(sys.stdout)
    f = IRCBotFactory(channels)
    reactor.connectTCP("irc.freenode.net", 6666, f)
    reactor.run()
