from __future__ import with_statement

import twisted
from twisted.python import log

from datetime import datetime
import random, time, re, urllib

from contextlib import contextmanager


PREFIX = ','
SOURCE = 'http://repo.or.cz/w/lusty.git'

def handleMsg(bot, user, channel, msg):
    ignoreChannels = [bot.nickname, 'AUTH']
    if channel not in ignoreChannels:
        log.msg('handleMsg> %s/%s: %s' % (user, channel, msg))

        rest = prefixMatch(msg, [PREFIX, bot.nickname + ':'])
        if rest is not None:
            # Run COMMAND
            commands = Commands(bot, user, channel)
            commands.run(rest)

        # Run FILTERS
        filters = Filters(bot, user, channel)
        filters.run(msg)

def prefixMatch(string, prefixes):
    for prefix in prefixes:
        if string.startswith(prefix):
            return string[len(prefix):]
    else:
        return None

@contextmanager
def probability_of_one_by(n):
    random.seed(time.time())
    yield 0 == random.choice(range(n))

def soupify(url):
    from BeautifulSoup import BeautifulSoup
    import urllib2
    req = urllib2.Request(url, None, {'User-agent': 'mozilla'})
    contents = urllib2.urlopen(req).read()
    return BeautifulSoup(contents)

def urlTitle(url):
    soup = soupify(url)
    return soup.title.contents[0].encode('utf-8').strip()

def decorate(proxy, original):
    # as decorator 'overwrites' namespace with these proxy
    # functions, we need to make the introspecting code happy.
    proxy.__doc__  = original.__doc__
    return proxy
    
def limitFreq(secs, msg=None):
    """
    Limit the frequency of execution of the function to avoid IRC spamming
    """
    lastmsg = {}
    def decorator(func):
        def proxy(self, arg=None):
            now = datetime.now()
            last = lastmsg.get(self.channel, None)
            if last is not None:
                diff = now - last
                if diff.seconds < secs:
                    log.msg('too quick!')
                    if msg:
                        self.toUser(msg)
                    return
                
            func(self, arg)
            lastmsg[self.channel] = now
        return decorate(proxy, func)
    
    return decorator

def admin(nicks):
    """
    Only admin listed in `nicks` can run this command. Silently fail otherwise.
    """
    def decorator(func):
        def proxy(self, *args, **kwargs):
            if self.user in nicks:
                return func(self, *args, **kwargs)
            else:
                log.msg('ADMIN: %s not authorized to run command %s' % (self.user, func.__name__))
            
        return decorate(proxy, func)
    return decorator

class BotActions(object):

    def __init__(self, bot):
        self.bot = bot

    def toChannel(self, msg):
        self.bot.msg(self.channel, msg)

    def toUser(self, msg):
        self.toChannel('%s: %s' % (self.user, msg))

    def doAction(self, action):
        self.bot.me(self.channel, action)

    def grep(self, msg, words):
        for word in words:
            # XXX: this does not search for a word, but substring!
            if word in msg:
                return True
        return False


class Commands(BotActions):

    def __init__(self, bot, user, channel):
        BotActions.__init__(self, bot)
        self.user = user
        self.channel = channel

    def run(self, rest):
        parts = rest.split(None, 1)
        command = parts[0]
        args = parts[1:]
        try:
            cmdFunc = getattr(self, 'cmd_%s' % command)
        except AttributeError:
            log.msg('no such command: %s', command)
        else:
            cmdFunc(*args)

    def cmd_ping(self, args=None):
        "respond to PING"
        self.toUser(args and 'PONG %s' % args or 'PONG')
    cmd_PING = cmd_ping

    @admin(['srid'])
    def cmd_reload(self, args=None):
        "reload plugins.py (self)"
        import plugins
        try:
            reload(plugins)
        except Exception, e:
            self.toUser(e)
        else:
            self.toUser('reloaded')
    cmd_r = cmd_reload

    def cmd_help(self, args=None):
        ignore = ('help', 'PING')
        for name, func, docstring in self:
            if name not in ignore and docstring is not None:
                self.toChannel('%10s - %s' % (name, docstring))

    # more useful commands

    def cmd_eval(self, args=None):
        "evaluate a python expression"
        if args:
            from math import * # make math functions available to `eval`
            try:
                result = eval(args)
            except SyntaxError, e:
                self.toUser('wrong syntax')
            except Exception, e:
                self.toUser(e)
            else:
                self.toChannel(result)
    cmd_e = cmd_eval

    def cmd_google(self, search_terms=None):
        "search the internet"
        if search_terms:
            soup = soupify('http://www.google.com/search?' + \
                               urllib.urlencode({'q': search_terms}))
            firstLink = soup.find('a', attrs={'class': 'l'})
            firstLink = firstLink['href'].encode('utf-8').strip()
            log.msg('google:', firstLink)
            self.toChannel(firstLink)
    cmd_g = cmd_google

    def cmd_source(self, args=None):
        "how to access lusty's source code"
        self.toChannel(SOURCE)

    def cmd_tell(self, args=None):
        if args:
            user, message = args.split(None, 1)
            # XXX: add to db
            self.toUser('ok')

    @admin(['srid'])
    def cmd_join(self, channel=None):
        if channel:
            self.bot.join(channel)

    @admin(['srid'])
    def cmd_leave(self, channel=None):
        if channel:
            self.bot.leave(channel)

    @admin(['srid'])
    def cmd_do(self, args):
        # perform an action in a channel
        # > ,do #foo is bored
        channel, action = args.split(None, 1)
        self.bot.me(channel, action)

    def __iter__(self):
        "Iterate over all commands (minus their aliases)"
        cmdMap = {}
        for attr in dir(self):
            if attr.startswith('cmd_'):
                func = getattr(self, attr)
                name = attr[len('cmd_'):]
                if func in cmdMap:
                    # find and overwrite the alias.
                    # the name of an alias is shorter than the name
                    # of the original version.
                    if len(cmdMap[func]) < len(name):
                        cmdMap[func] = name
                else:
                    cmdMap[func] = name
        
        for func, name in cmdMap.items():
            yield name, func, func.__doc__


class Filters(BotActions):

    def __init__(self, bot, user, channel):
        BotActions.__init__(self, bot)
        self.user = user
        self.channel = channel

    def run(self, msg):
        for attr in dir(self):
            if attr.startswith('filter_'):
                filter = getattr(self, attr)
                filter(msg)

    def respondWisely(self, msg, for_words, with_responses, one_for_every=1):
        random.seed(time.time())
        if self.grep(msg, for_words):
            with probability_of_one_by(one_for_every) as do:
                if do:
                    self.toChannel(random.choice(with_responses))

    def filter_obscene(self, msg):
        words = ['fuck', 'f**k', 'bitch', 'cunt', 'tits']
        quotes = [
            'The rate at which a person can mature is directly proportional to the embarrassment he can tolerate.',
            'To make mistakes is human; to stumble is commonplace; to be able to laugh at yourself is maturity.',
            'It is the province of knowledge to speak and it is the privilege of wisdom to listen.',
            'Some folks are wise and some otherwise.',
            'The wise man doesn\'t give the right answers, he poses the right questions.',
            'There are many who know many things, yet are lacking in wisdom.',
            'Wise men hear and see as little children do.',
            'Speech both conceals and reveals the thoughts of men',
            'Few people know how to take a walk. The qualifications are endurance, plain clothes, old shoes, an eye for nature, good humor, vast curiosity, good speech, good silence and nothing too much.',
            'The trouble with her is that she lacks the power of conversation but not the power of speech.',
            'Speak when you are angry - and you\'ll make the best speech you\'ll ever regret.',
            'Speech is human, silence is divine, yet also brutish and dead: therefore we must learn both arts.',
            'We must have reasons for speech but we need none for silence',
            'It usually takes more than three weeks to prepare a good impromptu speech.',
            'Men use thought only as authority for their injustice, and employ speech only to conceal their thoughts',
            'Much talking is the cause of danger. Silence is the means of avoiding misfortune. The talkative parrot is shut up in a cage. Other birds, without speech, fly freely about.',
            'Silence is also speech',
            'Speak properly, and in as few words as you can, but always plainly; for the end of speech is not ostentation, but to be understood.',
            ]

        self.respondWisely(msg, words, quotes)

    def filter_hatred(self, msg):
        words = ['hate', 'suck', 'bad']
        quotes = [
            'The first reaction to truth is hatred',
            'Hatred does not cease by hatred, but only by love; this is the eternal rule.',
            'Hatred is settled anger',
            'I love humanity but I hate people.',
            'Let them hate, so long as they fear.',
            'What we need is hatred. From it our ideas are born.',
            ]

        self.respondWisely(msg, words, quotes, 3)

    def filter_m8ball(self, msg):
        # This list is taken from fsbot@emacs (type ',dl m8ball')
        answers = [
            "Yes", "No", "Definitely", "Of course not!", "Highly likely.",
            "Ask yourself, do you really want to know?",
            "I'm telling you, you don't want to know.",
            "mu!"
            ]

        self.respondWisely(msg, ['??'], answers)

    def filter_random(self, msg):
        random.seed(time.time())
        with probability_of_one_by(100) as do:
            if do:
                with probability_of_one_by(2) as do2:
                    if do2:
                        self.doAction(
                            random.choice([
                                    'looks around nervously',
                                    ]))
                    else:
                        self.toChannel(
                            random.choice([
                                    'Let\'s fight a land war in space.'
                                    ]))

    _filter_links_url = re.compile('.*(https?://[^ ]*).*')
    def filter_links(self, msg):
        "Show <title> in channel"
        match = self._filter_links_url.match(msg)
        if match:
            self.toChannel('Title: %s' % urlTitle(match.group(1)))

